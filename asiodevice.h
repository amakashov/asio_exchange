#ifndef ASIODEVICE_H
#define ASIODEVICE_H

#include <iostream>
#include <array>
#include <thread>
#include <chrono>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/system_timer.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/signals2.hpp>
#include <vector>
#include <thread>

namespace asio=boost::asio;
namespace sign=boost::signals2;
namespace pt = boost::property_tree;

class ASIOContextHolder
{
public:
    ASIOContextHolder(const ASIOContextHolder&) = delete;
    ASIOContextHolder operator = (const ASIOContextHolder&) = delete;

    static asio::io_service& Instance();
private:
    ASIOContextHolder();// = default;
    ~ASIOContextHolder();
    void StartIOService();
    void ControlHandle(const boost::system::error_code &ec);

    asio::io_service m_service;
    asio::steady_timer m_controlTimer;
    std::thread m_ioThread;
};

class ASIODevice
{
public:
    ASIODevice();
    virtual ~ASIODevice();

    virtual int Write(std::vector<unsigned char> &array) = 0;
    virtual std::vector<unsigned char> Data();

    static int deviceCounter;
protected:
    struct diagnostic
    {
        unsigned int m_recvCounter;
        unsigned int m_sendCounter;
        unsigned int m_recvFreq;
        unsigned int m_sendFreq;
        unsigned int m_totalSend,
                     m_totalRecv;

        diagnostic() {m_recvFreq=m_sendFreq=m_totalRecv=m_totalSend=m_recvCounter=m_sendCounter=0;}
    } m_diagnostic;
    void ControlHandle(const boost::system::error_code &ec);

    virtual void ReadHandle (const boost::system::error_code &ec, std::size_t bytes_transferred) = 0;
    virtual void WriteHandle(const boost::system::error_code &ec) = 0;

    asio::io_service& m_service;
    asio::high_resolution_timer m_ctrlTimer;
    std::array<char, 1024> buffer;
    std::array<char, 40> m_writeBuffer;
    std::vector<unsigned char> m_recvBuffer;

    //  Develpment/Debugging stuff
    std::chrono::time_point<std::chrono::high_resolution_clock> m_tp;
    int m_number;


    sign::signal<void()> DataReady;
};

class ASIOSerial : public ASIODevice
{
public:
    ASIOSerial(pt::ptree& config);

    int Write(std::vector<unsigned char> &array);
//    std::vector<unsigned char> Data();

protected:
    void ReadHandle (const boost::system::error_code &ec, std::size_t bytes_transferred);
    void WriteHandle(const boost::system::error_code &ec);

    asio::serial_port m_port;

    //  Develpment/Debugging stuff
    asio::system_timer  m_writeTimer;
    unsigned int m_writeTimout = -1;
    bool m_master;
    int counter = 0;
};

class ASIOUdpSocket : public ASIODevice
{
public:
    ASIOUdpSocket(pt::ptree& config);

    int Write(std::vector<unsigned char> &array);
//    std::vector<unsigned char> Data();

protected:
    void ReadHandle (const boost::system::error_code &ec, std::size_t bytes_transferred);
    void WriteHandle(const boost::system::error_code &ec);

    asio::ip::udp::socket m_socket;
    asio::ip::udp::endpoint m_remoteEndpoint;

    //  Develpment/Debugging stuff
    asio::system_timer  m_writeTimer;
    unsigned int m_writeTimout = -1;
    bool m_master;
    int counter = 0;
};

class ASIOTCPClient : public ASIODevice
{
public:
    ASIOTCPClient();
    int Write(std::vector<unsigned char> &array);
protected:
    void ReadHandle (const boost::system::error_code &ec, std::size_t bytes_transferred);
    void WriteHandle(const boost::system::error_code &ec);

    void ConnectHandler(const boost::system::error_code &ec);

    std::shared_ptr<asio::ip::tcp::socket> m_socket;
    asio::ip::tcp::endpoint m_destination;


    //  Develpment/Debugging stuff
    asio::system_timer  m_writeTimer;
    unsigned int m_writeTimout = -1;
};

class ASIOTCPServer : public ASIODevice
{
public:
    ASIOTCPServer();
    int Write(std::vector<unsigned char> &array);
protected:
    void ReadHandle (const boost::system::error_code &ec, std::size_t bytes_transferred);
    void WriteHandle(const boost::system::error_code &ec);

    void AcceptHandler(const boost::system::error_code &ec);

    asio::ip::tcp::socket m_acceptingSocket;
    asio::ip::tcp::socket m_connectedSocket;
    asio::ip::tcp::endpoint m_destination;
    std::shared_ptr<asio::ip::tcp::acceptor> m_acceptor;
};

#endif // ASIODEVICE_H
