#include "asiodevice.h"
#include <functional>

//asio::io_service ioService;

using std::cout;
using std::endl;

asio::io_service &ASIOContextHolder::Instance()
{
    static ASIOContextHolder holder;
    return holder.m_service;
}

ASIOContextHolder::ASIOContextHolder()
    : m_controlTimer(m_service),  m_ioThread(std::bind(&ASIOContextHolder::StartIOService, this))
{
}

ASIOContextHolder::~ASIOContextHolder()
{
    m_service.stop();
    m_ioThread.join();
}

void ASIOContextHolder::StartIOService()
{
    m_controlTimer.async_wait(boost::bind(&ASIOContextHolder::ControlHandle, this, asio::placeholders::error()));
    m_controlTimer.expires_from_now(std::chrono::seconds(1));
//    m_controlTimer.expires_after(std::chrono::seconds(1));
//    asio::executor_work_guard<asio::io_service::executor_type> guard = asio::make_work_guard(m_service);
    m_service.run();
}

void ASIOContextHolder::ControlHandle(const boost::system::error_code &ec)
{
//    std::cout << "ASIOContextHolder tick!" << std::endl;
//    m_controlTimer.expires_after(std::chrono::seconds(1));
    m_controlTimer.expires_from_now(std::chrono::seconds(1));
    m_controlTimer.async_wait(boost::bind(&ASIOContextHolder::ControlHandle, this, asio::placeholders::error()));
}

int ASIODevice::deviceCounter = 0;

ASIODevice::ASIODevice()
    :   m_service(ASIOContextHolder::Instance()), m_ctrlTimer(m_service)
{
    m_number = ASIODevice::deviceCounter++;
    cout << "Creating device number " << m_number << endl;
    m_ctrlTimer.expires_from_now(std::chrono::milliseconds(1000));
    m_ctrlTimer.async_wait(boost::bind(&ASIODevice::ControlHandle, this,
                                       asio::placeholders::error()));
    buffer.fill(0);
}

ASIODevice::~ASIODevice()
{
    ASIODevice::deviceCounter--;
}

void ASIODevice::ControlHandle(const boost::system::error_code& ec)
{
    cout << "Tick! "<< m_number << endl;
    m_ctrlTimer.expires_from_now(std::chrono::milliseconds(1000));
    m_diagnostic.m_totalSend += m_diagnostic.m_recvCounter;
    m_diagnostic.m_totalRecv += m_diagnostic.m_sendCounter;
    m_diagnostic.m_recvFreq =   m_diagnostic.m_recvCounter;
    m_diagnostic.m_sendFreq =   m_diagnostic.m_sendCounter;
    m_diagnostic.m_recvCounter =m_diagnostic.m_sendCounter = 0;

    cout << "Send freq=" << m_diagnostic.m_sendFreq << "\tReceive freq=" << m_diagnostic.m_recvFreq << endl;
    m_ctrlTimer.async_wait(boost::bind(&ASIODevice::ControlHandle, this,
                                       asio::placeholders::error()));
}

std::vector<unsigned char> ASIODevice::Data()
{
    std::vector<unsigned char> ret = std::move(m_recvBuffer);
    return ret;
}


ASIOSerial::ASIOSerial(boost::property_tree::ptree &config)
    : ASIODevice (), m_port(m_service), m_writeTimer(m_service)
{
    auto devName = config.get("device", "COM1");

    m_port.open(devName);
//    std::string tmpString = "Sample message of hardly known size 40_b";
    std::string tmpString = "Sample_message_with_constant__40_bytes!!";
    std::copy(tmpString.begin(), tmpString.end(), m_writeBuffer.begin());
    cout << "Opening device reading device " << devName<< endl;
    if (m_port.is_open())
    {
        std::cout << "Reading port succesfully opened" << std::endl;
        m_port.set_option(asio::serial_port_base::baud_rate(config.get("speed",115200)));
        m_port.set_option(asio::serial_port_base::parity());
        m_port.set_option(asio::serial_port_base::stop_bits());
        m_port.set_option(asio::serial_port_base::character_size(config.get("data_bits", 8)));
        m_port.set_option(asio::serial_port_base::flow_control());
        m_port.async_read_some(asio::buffer(buffer),boost::bind(&ASIOSerial::ReadHandle, this,
                                                                       asio::placeholders::error(),
                                                                       asio::placeholders::bytes_transferred()));

        m_master = config.get("master", false);
        if (m_master)
        {
            cout << "Here goes master device!\n";
            m_writeTimout = 1000000 / config.get("frequency", 10);
            cout << "Starting write timer with timeout of " << m_writeTimout << " microseconds" << endl;
//            m_writeTimer.expires_after(std::chrono::microseconds(m_writeTimout));
            m_writeTimer.expires_from_now(std::chrono::microseconds(m_writeTimout));
            m_writeTimer.async_wait(boost::bind(&ASIOSerial::WriteHandle, this, asio::placeholders::error()));
        }
#ifdef _HAS_POSIX_C_LIB
        int readHandle = m_port.native_handle();
        if (readHandle>0 && !m_master)
        {
            cout <<"Handle acquired" << endl;
            struct termios termios_p;
            if (!tcgetattr(readHandle, & termios_p))
            {
                cout << "Attributes reachable" << endl;
                termios_p.c_cc[VMIN] = 40;
                termios_p.c_cc[VTIME] = 0;
                termios_p.c_lflag &= ~(ECHO|ICANON|ISIG|ECHOE|ECHOK|ECHONL);
                termios_p.c_oflag &= ~(OPOST);
                bool res = tcsetattr(readHandle, TCSADRAIN, &termios_p);
                if (!res)
                    cout << "New attributes succesfully set" << endl;
                else
                    cout << "Error on attribuutes setting" << endl;
            }
        }
#endif

    }
    else
    {
        cout << "Unable to open reading device" << endl;
    }
}

int ASIOSerial::Write(std::vector<unsigned char>& array)
{
    size_t bytesWriten= m_port.write_some(asio::buffer(array));
    return bytesWriten;
}

void ASIOSerial::ReadHandle(const boost::system::error_code& ec, std::size_t bytes_transferred)
{
    if (!ec)
    {

        //  TODO: Development stuff - delete
        std::string answer= "Rogger!";
        if (!m_master)
            m_port.write_some(asio::buffer(answer));
//        if (m_master)
//            cout << buffer.data() << endl;
        //

        m_diagnostic.m_recvCounter++;
        if (m_recvBuffer.size()>2048)
            m_recvBuffer.clear();
        m_recvBuffer.insert(m_recvBuffer.end(), buffer.begin(), buffer.begin()+bytes_transferred);
        m_port.async_read_some(asio::buffer(buffer),
                             boost::bind(&ASIOSerial::ReadHandle, this,
                                         asio::placeholders::error(),
                                         asio::placeholders::bytes_transferred()));
        DataReady();
    }
    else
    {
        std::cerr << ec.message() << endl;
    }

}

void ASIOSerial::WriteHandle(const boost::system::error_code &ec)
{
//    m_writeTimer.expires_after(std::chrono::microseconds(m_writeTimout));
    m_writeTimer.expires_from_now(std::chrono::microseconds(m_writeTimout));
    m_writeTimer.async_wait(boost::bind(&ASIOSerial::WriteHandle, this,
                                        asio::placeholders::error()));
    std::vector<unsigned char> array;
    array.reserve(40);
    std::copy(m_writeBuffer.begin(),m_writeBuffer.end(),array.begin());
    Write(array);
    m_port.write_some(asio::buffer(m_writeBuffer));
    m_diagnostic.m_sendCounter++;
}

ASIOUdpSocket::ASIOUdpSocket(boost::property_tree::ptree &config)
    :   ASIODevice(), m_socket(m_service), m_writeTimer(m_service)
{
//    asio::ip::address addr = asio::ip::address::make_address("127.0.0.1");
    asio::ip::address addr = asio::ip::address_v4();
    if (config.find("address") != config.not_found())
        addr = asio::ip::address::from_string(config.get("adress", "127.0.0.1"));
    int port = config.get("port", 27001);
    m_socket.open(asio::ip::udp::v4());
    cout << "Binding UDP socket to " << addr.to_string() << ":" << port << endl;
    m_socket.bind(asio::ip::udp::endpoint(addr, port));

    m_socket.async_receive_from(asio::buffer(buffer), m_remoteEndpoint,
                                boost::bind(&ASIOUdpSocket::ReadHandle, this,
                                            asio::placeholders::error(),
                                            asio::placeholders::bytes_transferred()));

    m_master = config.get("master", false);
    std::string tmpString = "Sample_message_with_constant__40_bytes!!";
    std::copy(tmpString.begin(), tmpString.end(), m_writeBuffer.begin());
    if (m_master)
    {
        addr = asio::ip::address::from_string(config.get("send_address", "127.0.0.1"));
        port = config.get("send_port", 27002);
        m_remoteEndpoint.address(addr);
        m_remoteEndpoint.port(port);
        cout << "Here goes master device!\n";
        m_writeTimout = 1000000 / config.get("frequency", 10);
        cout << "Starting write timer with timeout of " << m_writeTimout << " microseconds" << endl;
//            m_writeTimer.expires_after(std::chrono::microseconds(m_writeTimout));
        m_writeTimer.expires_from_now(std::chrono::microseconds(m_writeTimout));
        m_writeTimer.async_wait(boost::bind(&ASIOUdpSocket::WriteHandle, this, asio::placeholders::error()));
    }
}

void ASIOUdpSocket::ReadHandle(const boost::system::error_code &ec, std::size_t bytes_transferred)
{
    if (!ec)
    {
        //  TODO: Development stuff - delete
        std::string answer= "Rogger on UDP!";
        if (!m_master)
        {
            m_socket.send_to(asio::buffer(answer),m_remoteEndpoint);
        }
        //

        m_diagnostic.m_recvCounter++;
        if (m_recvBuffer.size()>2048)
            m_recvBuffer.clear();
        m_recvBuffer.insert(m_recvBuffer.end(), buffer.begin(), buffer.begin()+bytes_transferred);
        m_socket.async_receive_from(asio::buffer(m_recvBuffer), m_remoteEndpoint,
                                    boost::bind(&ASIOUdpSocket::ReadHandle, this,
                                                asio::placeholders::error(),
                                                asio::placeholders::bytes_transferred()));
        DataReady();
    }
    else
    {
        std::cerr << ec.message() << endl;
    }
}

void ASIOUdpSocket::WriteHandle(const boost::system::error_code &ec)
{
//    m_writeTimer.expires_after(std::chrono::microseconds(m_writeTimout));
    m_writeTimer.expires_from_now(std::chrono::microseconds(m_writeTimout));
    std::vector<unsigned char> array;
    array.reserve(40);
    std::copy(m_writeBuffer.begin(),m_writeBuffer.end(),array.begin());
    size_t bytesWriten = Write(array);
    m_diagnostic.m_sendCounter++;
    m_writeTimer.async_wait(boost::bind(&ASIOUdpSocket::WriteHandle, this,
                                        asio::placeholders::error()));
}

int ASIOUdpSocket::Write(std::vector<unsigned char>& array)
{
    size_t bytesWriten = m_socket.send_to(asio::buffer(m_writeBuffer), m_remoteEndpoint);
    return bytesWriten;
}

ASIOTCPClient::ASIOTCPClient()
    : m_writeTimer(m_service)
{
    m_socket = std::make_shared<asio::ip::tcp::socket>(m_service);
    asio::ip::address_v4 addr = asio::ip::address_v4::from_string("127.0.0.1");
    int port = 27003;

    m_destination.port(port);
    m_destination.address(addr);

    m_socket->async_connect(m_destination, boost::bind(&ASIOTCPClient::ConnectHandler, this,
                                                      asio::placeholders::error()));

    std::string tmpString = "Sample_message_with_constant__40_bytes!!";
    std::copy(tmpString.begin(), tmpString.end(), m_writeBuffer.begin());
    cout << "Here goes master device!\n";
    m_writeTimout = 1000000 / 100;
}

int ASIOTCPClient::Write(std::vector<unsigned char> &array)
{
    size_t bytesWriten = m_socket->send(asio::buffer(m_writeBuffer));
    return bytesWriten;
}

void ASIOTCPClient::ReadHandle(const boost::system::error_code &ec, std::size_t bytes_transferred)
{
    m_diagnostic.m_recvCounter++;
    cout << buffer.data() << endl;
    if (m_recvBuffer.size()>2048)
        m_recvBuffer.clear();
    m_recvBuffer.insert(m_recvBuffer.end(), buffer.begin(), buffer.begin()+bytes_transferred);
    m_socket->async_receive(asio::buffer(buffer),
                                boost::bind(&ASIOTCPClient::ReadHandle, this,
                                            asio::placeholders::error(),
                                            asio::placeholders::bytes_transferred()));
    DataReady();
}

void ASIOTCPClient::WriteHandle(const boost::system::error_code &ec)
{
    std::vector<unsigned char> array;
    array.reserve(40);
    std::copy(m_writeBuffer.begin(),m_writeBuffer.end(),array.begin());
    size_t size = Write(array);
//    size_t size = m_socket->write_some(asio::buffer(m_writeBuffer));
//    cout << "Writing " << array.data() << " of "<< size << " bytes" << endl;
    m_diagnostic.m_sendCounter++;
    m_writeTimer.expires_from_now(std::chrono::microseconds(m_writeTimout));
    m_writeTimer.async_wait(boost::bind(&ASIOTCPClient::WriteHandle, this,
                                        asio::placeholders::error()));
}

void ASIOTCPClient::ConnectHandler(const boost::system::error_code &ec)
{
    if (!ec)
    {
        cout << "Starting write timer with timeout of " << m_writeTimout << " microseconds" << endl;
        m_writeTimer.expires_from_now(std::chrono::microseconds(m_writeTimout));
        m_writeTimer.async_wait(boost::bind(&ASIOTCPClient::WriteHandle, this, asio::placeholders::error()));

        m_socket->async_receive(asio::buffer(buffer),
                               boost::bind(&ASIOTCPClient::ReadHandle, this,
                                           asio::placeholders::error(),
                                           asio::placeholders::bytes_transferred()));
    }
    else if (ec.value()==boost::system::errc::connection_refused)
    {
        cout << "TCP_CLIENT: " << ec.message() << endl;
        cout << m_destination.address().to_string() << ":" << m_destination.port() << endl;
        m_socket.reset(new asio::ip::tcp::socket(m_service));
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        m_socket->async_connect(m_destination, boost::bind(&ASIOTCPClient::ConnectHandler, this,
                                                          asio::placeholders::error()));

    }
    else
    {
        cout << "TCP_CLIENT: " << ec.message() << endl;
    }
}

ASIOTCPServer::ASIOTCPServer()
    : m_acceptingSocket(m_service), m_connectedSocket(m_service)
{
    asio::ip::address_v4 addr = asio::ip::address_v4::from_string("127.0.0.1");
    int port = 27003;

    m_destination.port(port);
    m_destination.address(asio::ip::address_v4());

    asio::ip::tcp::endpoint endpoint(asio::ip::tcp::v4(), port);

    m_acceptor = std::make_shared<asio::ip::tcp::acceptor>(m_service, endpoint);

    cout << "TCP_SERVER: Running accept on " << m_acceptor->local_endpoint().address() <<":" << m_acceptor->local_endpoint().port() << endl;
    m_acceptor->async_accept(m_acceptingSocket, boost::bind(&ASIOTCPServer::AcceptHandler, this,
                                                           asio::placeholders::error()));
}

int ASIOTCPServer::Write(std::vector<unsigned char> &array)
{
    size_t bytesWriten = m_acceptingSocket.send(asio::buffer(m_writeBuffer));
    return bytesWriten;
}

void ASIOTCPServer::ReadHandle(const boost::system::error_code &ec, std::size_t bytes_transferred)
{
    m_diagnostic.m_recvCounter++;
    cout << "TCP_SERVER: Got " <<buffer.data() << " with size " << bytes_transferred << endl;
    if (m_recvBuffer.size()>2048)
        m_recvBuffer.clear();
    m_recvBuffer.insert(m_recvBuffer.end(), buffer.begin(), buffer.begin()+bytes_transferred);
    m_acceptingSocket.async_receive(asio::buffer(buffer),
                                boost::bind(&ASIOTCPServer::ReadHandle, this,
                                            asio::placeholders::error(),
                                            asio::placeholders::bytes_transferred()));
    DataReady();
    std::string answer= "Rogger on UDP!";
    m_acceptingSocket.send(asio::buffer(answer));
}

void ASIOTCPServer::WriteHandle(const boost::system::error_code &ec)
{

}

void ASIOTCPServer::AcceptHandler(const boost::system::error_code &ec)
{
    std::cout << "TCP_SERVER: Connection accepted " << ec.message() << endl;
    m_acceptingSocket.async_receive(asio::buffer(buffer),
                           boost::bind(&ASIOTCPServer::ReadHandle, this,
                                       asio::placeholders::error(),
                                       asio::placeholders::bytes_transferred()));
}
